# API for Daily List of HouseHolds with Daily Tasks for PAs
A simple mocked REST API based on JSON Placeholder, to be used/ consumed by a the Angular front-end for Task List app.


![Task List Prototype API 1](http://www.ryanhunter.ca/images/portfolio/single_source_api/screen01.png)



## Steps to Install 
- Run the command below from the command line / terminal / command prompt.
- git clone https://systemsvanguard@bitbucket.org/systemsvanguard/api_tasklist_json.git  
- cd api_tasklist_json  
- ensure your have Node & NPM pre-installed. 
- Run commands 'node --version && npm -v'.
- npm install.  (This ensures all dependencies are installed).
- npm start
- Runs on port 4000 via .env variable --> http://localhost:4000/ 
- Change the web port as needed.


## Features
- Node.js
- JSON Placeholder
- RESTful API
- JSON 


## License
This project is licensed under the terms of the **MIT** license.


## Screenshots 

![Task List Prototype API 1](http://www.ryanhunter.ca/images/portfolio/single_source_api/screen01.png)


![Task List Prototype API 2](http://www.ryanhunter.ca/images/portfolio/single_source_api/screen02.png)

